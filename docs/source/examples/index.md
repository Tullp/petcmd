
# Examples

Here are a few petcmd usage examples.
Check the [git repo](https://gitlab.com/Tullp/petcmd/-/tree/master/examples) for more.

```{toctree}
:maxdepth: 2

demo.md
tutorial.md
```
