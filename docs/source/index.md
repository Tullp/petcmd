
# Welcome to petcmd!

![PyPI - Python Version](https://img.shields.io/pypi/pyversions/petcmd)
![PyPI - Version](https://img.shields.io/pypi/v/petcmd)
![Documentation Status](https://readthedocs.org/projects/petcmd/badge/?version=latest)
![PyPI - License](https://img.shields.io/pypi/l/petcmd)
![Downloads](https://static.pepy.tech/badge/petcmd)

## About

**petcmd** provides the ability to automatically create a CLI interface. It was created to reduce to zero unnecessary
steps for developers to create a CLI interface so that they could focus on the functionality of the application.
All existing libraries have a complex API and require a complex configuration of command parameters,
while in **petcmd** the entire API consists only of a decorator for marking commands and an entrypoint.
It also doesn't have any third-party dependencies.

* * *

## Contents

```{toctree}
:maxdepth: 2

getting-started.md
tutorial.md
examples/index.md
```
