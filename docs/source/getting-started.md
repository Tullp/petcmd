
# Getting Started

## Installing with pip

We recommend using [pip](http://pypi.python.org/pypi/pip) to install petcmd on all platforms:

```bash
$ python -m pip install petcmd
```

To get a specific version of petcmd:

```bash
$ python -m pip install petcmd==0.2.3
```

To upgrade using pip:

```bash
$ python -m pip install --upgrade petcmd
```

## Installing from source

You can also download the project source and install it:

```bash
$ git clone https://gitlab.com/tullp/petcmd.git
$ cd petcmd/
$ pip install .
```
