
from petcmd.commander import Commander
from petcmd.exceptions import CommandException

def run():

	def test(func):
		Commander().command("cmd")(func)

	def test_exception(func, message: str):
		try:
			Commander().command("cmd")(func)
			assert False, f"Should raise exception with message: {message}"
		except CommandException as e:
			assert e.args[0] == message, f"Should raise exception with message: {message}"

	def f1(): pass
	def f2(): pass
	commander = Commander()
	commander.command("cmd1")(f1)
	try:
		commander.command("cmd1")(f2)
		assert False, "Should raise duplicate command exception"
	except CommandException as e:
		assert e.args[0] == "Duplicated command: cmd1"

	def func(): pass
	test(func)

	for argtype in (str, int, float, bool, list, tuple, set, dict):
		# noinspection PyUnusedLocal
		def func(a: argtype): pass
		test(func)

	# noinspection PyUnusedLocal
	def func(a: bytes): pass
	test_exception(func, "Unsupported typehint: petcmd supports only next types: "
						"str, int, float, bool, list, tuple, set, dict, PipeOutput")

	for argtype in (str, int, float, bool, list, tuple, set, dict):
		# noinspection PyUnusedLocal
		def func(a: list[argtype]): pass
		test(func)

	# noinspection PyUnusedLocal
	def func(a: list[bytes]): pass
	test_exception(func, "Unsupported typehint generic: petcmd supports only basic generics")

	# noinspection PyUnusedLocal
	def func(a: list[int | str]): pass
	test_exception(func, "Unsupported typehint generic: petcmd supports only basic generics")


