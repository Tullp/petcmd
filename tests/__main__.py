
from tests.value_parsing import run as value_parsing
from tests.args_parsing import run as args_parsing
from tests.command_matching import run as command_matching
from tests.commands_validation import run as commands_validation

value_parsing()
args_parsing()
command_matching()
commands_validation()
