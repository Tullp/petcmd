
from petcmd.command import Command
from petcmd.argparser import ArgParser
from petcmd.exceptions import CommandException

# noinspection PyUnresolvedReferences,PyProtectedMember
def run():

	def test(argv: str | list, expect):
		assert ArgParser.parse(argv.split() if isinstance(argv, str) else argv, command) == expect, \
			f"Wrong arguments parsing: {argv} should be parsed to {expect}"

	def test_exception(argv: str | list, message):
		try:
			ArgParser.parse(argv.split() if isinstance(argv, str) else argv, command)
			assert False, f"Should raise exception with message: {message}"
		except CommandException as e:
			assert e.args[0] == message, f"Should raise exception with message: {message}"

	def func(): pass
	command = Command("test", func)
	test([], ([], {}))

	# noinspection PyUnusedLocal
	def func(arg: str = None): pass
	command = Command("test", func)
	test("test", ([], {"arg": "test"}))
	test("-a test", ([], {"arg": "test"}))
	test("--arg test", ([], {"arg": "test"}))

	# noinspection PyUnusedLocal
	def func(arg: str): pass
	command = Command("test", func)
	test("test", (["test"], {}))
	test("-a test", (["test"], {}))
	test("--arg test", (["test"], {}))

	# noinspection PyUnusedLocal
	def func(a, b, c): pass
	command = Command("test", func)
	test( "1 2 3", (["1", "2", "3"], {}))
	test("1 2 -c 3", (["1", "2", "3"], {}))
	test("1 -b 2 -c 3", (["1", "2", "3"], {}))
	test("-a 1 -b 2 -c 3", (["1", "2", "3"], {}))
	test_exception("1 -b 2", "Invalid usage: missing required positional arguments")
	test_exception("1 -c 2", "Invalid usage: missing required positional arguments")
	test_exception("0 -a 1 -b 2 -c 3", "Invalid usage: unexpected number of positional arguments")
	test_exception("1 2 3 4", "Invalid usage: unexpected number of positional arguments")
	test_exception("1 2 3 -d 4", "Invalid usage: unexpected number of keyword arguments")
	test_exception("1 2 -d", "Invalid usage: missing d option value")
	test_exception("1 2 -d -t", "Invalid usage: missing d option value")
	test_exception("1 2 -b 3", "Invalid usage: positional argument 'c' follows keyword argument 'b'")

	# noinspection PyUnusedLocal
	def func(a: int, b: bool, c: str, d: int = 1, e: bool = False, f: str = "i"): pass
	command = Command("test", func)
	test("-a 1 -b true -c 3", ([1, True, "3"], {}))
	test("1 1 z -d 2 -f j", ([1, True, "z"], {"d": 2, "f": "j"}))
	test("1 1 -d 2 -c z -f j", ([1, True, "z"], {"d": 2, "f": "j"}))
	test("1 1 -e -c z -f j", ([1, True, "z"], {"e": True, "f": "j"}))
	test("-e -c z -f j 1 true", ([1, True, "z"], {"e": True, "f": "j"}))
	test("1 0 x 5 true y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("1 0 x 5 true -f y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("1 0 x 5 -e -f y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("1 0 x -d 5 -e -f y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("1 0 -c x -d 5 -e -f y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("1 -b 0 -c x -d 5 -e -f y", ([1, False, "x"], {"d": 5, "e": True, "f": "y"}))
	test("-a 1 -b 1 -c x -d 5 -e -f y", ([1, True, "x"], {"d": 5, "e": True, "f": "y"}))

	# noinspection PyUnusedLocal
	def func(a: int, b: bool): pass
	command = Command("test", func)
	test("1 1", ([1, True], {}))
	test("-a 2 -b 0", ([2, False], {}))
	test_exception("-a 2 -b", "Invalid usage: missing b option value")

	# noinspection PyUnusedLocal
	def func(a: int, b: bool = True): pass
	command = Command("test", func)
	test_exception("-a 2 -b", "Invalid usage: missing b option value")

	# noinspection PyUnusedLocal
	def func(a: int, b: bool = False): pass
	command = Command("test", func)
	test("-a 2 -b", ([2], {"b": True}))

	# noinspection PyUnusedLocal
	def func(a: int, b: list, c: str): pass
	command = Command("test", func)
	test("-a 1 -b z x y -c q", ([1, ["z", "x", "y"], "q"], {}))
	test("-b z x y -a 1 -c q", ([1, ["z", "x", "y"], "q"], {}))

	# noinspection PyUnusedLocal
	def func(s: int, a: int, b: list, c: str): pass
	command = Command("test", func)
	test("-a 1 -c q 50 -b z x y", ([50, 1, ["z", "x", "y"], "q"], {}))

	# noinspection PyUnusedLocal
	def func(a: int, b: tuple, c: str): pass
	command = Command("test", func)
	test("-a 1 -b z x y -c q", ([1, ("z", "x", "y"), "q"], {}))
	test("-b z x y -a 1 -c q", ([1, ("z", "x", "y"), "q"], {}))

	# noinspection PyUnusedLocal
	def func(s: int, a: int, b: tuple, c: str): pass
	command = Command("test", func)
	test("-a 1 -c q 50 -b z x y", ([50, 1, ("z", "x", "y"), "q"], {}))

	# noinspection PyUnusedLocal
	def func(a: int, b: set, c: str): pass
	command = Command("test", func)
	test("-a 1 -b z x y -c q", ([1, {"z", "x", "y"}, "q"], {}))
	test("-b z x y -a 1 -c q", ([1, {"z", "x", "y"}, "q"], {}))

	# noinspection PyUnusedLocal
	def func(s: int, a: int, b: set, c: str): pass
	command = Command("test", func)
	test("-a 1 -c q 50 -b z x y", ([50, 1, {"z", "x", "y"}, "q"], {}))

	# noinspection PyUnusedLocal
	def func(arr: list[list]): pass
	command = Command("test", func)
	test("--arr [1,2,3] [4,5,6] [7,8,9]", ([[[1, 2, 3], [4, 5, 6], [7, 8, 9]]], {}))

	# noinspection PyUnusedLocal
	def func(arr: list[list] = None): pass
	command = Command("test", func)
	test("--arr [1,2,3] [4,5,6] [7,8,9]", ([], {"arr": [[1, 2, 3], [4, 5, 6], [7, 8, 9]]}))

	# noinspection PyUnusedLocal
	def func(d: dict[str, list]): pass
	command = Command("test", func)
	test("-d abc=[1,2,3] xyz=[4,5,6] qwe=[7,8,9]", ([{"abc": [1, 2, 3], "xyz": [4, 5, 6], "qwe": [7, 8, 9]}], {}))
	test_exception("abc=[1,2,3] xyz=[4,5,6] qwe=[7,8,9]", "Invalid usage: unexpected number of positional arguments")

	# noinspection PyUnusedLocal
	def func(*args): pass
	command = Command("test", func)
	test([], ([], {}))
	test("abc xyz qwe", (["abc", "xyz", "qwe"], {}))
	test_exception("abc xyz -d qwe", "Invalid usage: unexpected number of keyword arguments")

	# noinspection PyUnusedLocal
	def func(a: int, b: str, *args, c: int = 10, d: str = "z"): pass
	command = Command("test", func)
	test("1 q 5 w", ([1, "q", "5", "w"], {}))
	test("1 q -c 5 -d w", ([1, "q"], {"c": 5, "d": "w"}))
	test("1 q i j -c 5 -d w", ([1, "q", "i", "j"], {"c": 5, "d": "w"}))
	test("1 -b q i j -c 5 -d w", ([1, "q", "i", "j"], {"c": 5, "d": "w"}))
	test("-a 1 -b q i j -c 5 -d w", ([1, "q", "i", "j"], {"c": 5, "d": "w"}))
	test_exception("-a 1 q i j -c 5 -d w", "Invalid usage: positional argument 'b' follows keyword argument 'a'")
	test("-a 1 -b q i j", ([1, "q", "i", "j"], {}))
	test("1 -b q i j", ([1, "q", "i", "j"], {}))

	# noinspection PyUnusedLocal
	def func(a: int, b: str, c: int = 10, d: str = "z"): pass
	command = Command("test", func)
	test("5 -b z -d q", ([5, "z"], {"d": "q"}))
	test("5 z 20 -d q", ([5, "z"], {"c": 20, "d": "q"}))
	test_exception("5 20 -b z -d q", "Invalid usage: unexpected number of positional arguments")
	test("5 z 20 q", ([5, "z"], {"c": 20, "d": "q"}))
	test_exception("5 z 20 q x", "Invalid usage: unexpected number of positional arguments")
	test("5 z 20", ([5, "z"], {"c": 20}))
	test_exception("5 z 20 -c 50", "Invalid usage: keyword argument c have been specified as positional already")
	test("5 z 20 q", ([5, "z"], {"c": 20, "d": "q"}))
	test_exception("5 z 20 q -c 50", "Invalid usage: keyword argument c have been specified as positional already")
	test_exception("5 z 20 q -d q", "Invalid usage: keyword argument d have been specified as positional already")
