
from petcmd.argparser import ArgParser
from petcmd.exceptions import CommandException

def run():

	# noinspection PyUnresolvedReferences,PyProtectedMember
	def test(value, typehint, expect):
		assert ArgParser._ArgParser__parse_value(value, typehint) == expect, \
			f"Wrong value parsing: {value} -> {typehint} should be {expect}"

	# noinspection PyUnresolvedReferences,PyProtectedMember
	def test_exception(value, typehint, message):
		try:
			ArgParser._ArgParser__parse_value(value, typehint)
			assert False, f"Should raise exception with message: {message}"
		except CommandException as e:
			assert e.args[0] == message, f"Should raise exception with message: {message}"

	test("abc", str, "abc")
	test("abc", None, "abc")

	test("123", int, 123)
	test("00123", int, 123)
	test_exception("123.456", int, "123.456 can't be converted to <class 'int'>")

	test("123.1", float, 123.1)
	test_exception("123.1.2", float, "123.1.2 can't be converted to <class 'float'>")

	test("true", bool, True)
	test("True", bool, True)
	test("1", bool, True)
	test("false", bool, False)
	test("False", bool, False)
	test("0", bool, False)
	test_exception("2", bool, "2 can't be converted to <class 'bool'>")
	test_exception("a", bool, "a can't be converted to <class 'bool'>")
	test_exception("", bool, " can't be converted to <class 'bool'>")
	test_exception("10", bool, "10 can't be converted to <class 'bool'>")
	test_exception("null", bool, "null can't be converted to <class 'bool'>")

	test('{"a": 5}', dict, {"a": 5})
	test('{"a": 5, "b": [{"c": 10}]}', dict, {"a": 5, "b": [{"c": 10}]})
	test("{'a': 5}", dict, {"a": 5})
	test_exception("[1, 2, 3]", dict, "[1, 2, 3] can't be converted to <class 'dict'>")

	test('[1, 2, 3]', list, [1, 2, 3])
	test('[1, "2", True]', list, [1, "2", True])
	test_exception("123", list, "123 can't be converted to <class 'list'>")

	test('(1, 2, 3)', tuple, (1, 2, 3))
	test('(1, "2", True)', tuple, (1, "2", True))
	test_exception("123", tuple, "123 can't be converted to <class 'tuple'>")

	test('{1, 2, 3}', set, {1, 2, 3})
	test('{1, "2", True}', set, {1, "2", True})
	test_exception("123", set, "123 can't be converted to <class 'set'>")

	test(["1", "2", "3"], list, ["1", "2", "3"])
	test(["1", "2", "3", "2"], set, {"1", "2", "3"})
	test(["1", "2", "3", "2"], tuple, ("1", "2", "3", "2"))
	test(["1", "2", "3"], list[str], ["1", "2", "3"])
	test(["1", "true", "0", "False"], list[bool], [True, True, False, False])
	test(["1", "2", "3"], list[int], [1, 2, 3])
	test(["1", "2", "3", "1"], set[int], {1, 2, 3})
	test(["1", "2", "3", "5"], tuple[int], (1, 2, 3, 5))
	test(["1", "2", "3", "5"], tuple[int, str, str, int], (1, "2", "3", 5))
	test_exception(["1", "2", "3", "5"], tuple[int, str, str, int, bool],
		"Mismatch between the number of elements and tuple generic types")
	test_exception(["1", "2", "3", "5"], tuple[int, str, str],
		"Mismatch between the number of elements and tuple generic types")

	test({"a": "1", "5": "False"}, dict, {"a": "1", "5": "False"})
	test_exception({"a": "1", "5": "False"}, dict[str], "Invalid number of dict generic types, should be 2")
	test({"a": "1", "5": "False"}, dict[str, bool], {"a": True, "5": False})
	test({"10": "[1,2,3]", "5": "[False,2]"}, dict[int, list], {10: [1, 2, 3], 5: [False, 2]})
