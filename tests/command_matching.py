
from petcmd.command import Command

def run():

	def matching(cmd1: str | tuple[str, ...], cmd2: str | tuple[str, ...]):
		assert Command((cmd1,) if isinstance(cmd1, str) else cmd1, lambda: 0).match(cmd2), \
			f"Commands should matching: {cmd1}, {cmd2}"

	def not_matching(cmd1: str | tuple[str, ...], cmd2: str | tuple[str, ...]):
		assert not Command((cmd1,) if isinstance(cmd1, str) else cmd1, lambda: 0).match(cmd2), \
			f"Commands shouldn't matching: {cmd1}, {cmd2}"

	# str to str
	matching("cmd", "cmd")
	not_matching("cmd1", "cmd2")
	not_matching("cmd1", "cmd")
	not_matching("cmd1", "")

	# list to str
	matching(("cmd1",), "cmd1")
	not_matching(("cmd1",), "cmd")
	not_matching(("cmd1",), "cmd2")
	matching(("cmd1", "cmd2"), "cmd1")
	matching(("cmd1", "cmd2"), "cmd2")
	not_matching(("cmd1", "cmd2"), "cmd")
	matching(("cmd1", "cmd2", "cmd3"), "cmd1")
	matching(("cmd1", "cmd2", "cmd3"), "cmd2")
	matching(("cmd1", "cmd2", "cmd3"), "cmd3")
	not_matching(("cmd1", "cmd2", "cmd3"), "")
	not_matching(("cmd1", "cmd2", "cmd3"), "cmd")
	not_matching(("cmd1", "cmd2", "cmd3"), "cmd4")

	# str to list
	matching("cmd1", ("cmd1",))
	not_matching("cmd", ("cmd1",))
	not_matching("cmd2", ("cmd1",))
	matching("cmd1", ("cmd1", "cmd2"))
	matching("cmd2", ("cmd1", "cmd2"))
	not_matching("cmd", ("cmd1", "cmd2"))
	matching("cmd1", ("cmd1", "cmd2", "cmd3"))
	matching("cmd2", ("cmd1", "cmd2", "cmd3"))
	matching("cmd3", ("cmd1", "cmd2", "cmd3"))
	not_matching(("cmd1", "cmd2", "cmd3"), "")
	not_matching("cmd", ("cmd1", "cmd2", "cmd3"))
	not_matching("cmd4", ("cmd1", "cmd2", "cmd3"))

	# list to list
	matching(("cmd1",), ("cmd1",))
	matching(("cmd1",), ("cmd1", "cmd2"))
	matching(("cmd1", "cmd2"), ("cmd1",))
	matching(("cmd1", "cmd2"), ("cmd1", "cmd3"))
	matching(("cmd1", "cmd2"), ("cmd1", "cmd2"))
	not_matching(("cmd1",), ("cmd2",))
	not_matching(("cmd1", "cmd2"), ("cmd3",))
	not_matching(("cmd1",), ("cmd2", "cmd3"))
	not_matching(("cmd1", "cmd2"), ("cmd3", "cmd4"))
